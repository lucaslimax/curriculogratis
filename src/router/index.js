import Vue from 'vue'
import Router from 'vue-router'

import Curriculo from '@/components/pages/Curriculo'
import About from '@/components/pages/About'
import Foundation from '@/components/pages/Foundation'
import Donate from '@/components/pages/Donate'
import Version2 from '@/components/pages/Version2'

import NotFound from '@/components/pages/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Curriculo', component: Curriculo },
    { path: '/about', name: 'About', component: About },
    { path: '/foundation', name: 'Foundation', component: Foundation },
    { path: '/donate', name: 'Donate', component: Donate },
    { path: '/v2', name: 'Version2', component: Version2 },
    { path: '/*', name: 'NotFound', component: NotFound }
  ],
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})
