'use strict'

const fs = require('fs')
const UglifyJS = require('uglify-es')

module.exports = function(filePath) {
  let code = fs.readFileSync(filePath, 'utf-8'),
      result = UglifyJS.minify(code)
  if (result.error) return ''
  return result.code
}
