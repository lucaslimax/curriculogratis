#!/bin/bash
echo "########################################"
echo "build cross - firebase/heroku"
echo "########################################"
echo "----------------------------------------"
time_now=$(date)
main()
{
  sudo rm -rf dist
  echo "########################################"
  echo "Clean last build - DONE"
  npm run build
  echo "########################################"
  echo "Build - DONE"
}
deploy_heroku()
{
  echo "########################################"
  echo "Inicializando deploy heroku"
  sudo rm -rf ../curriculo-gratis/dist/
  echo "########################################"
  echo "Clean dist - DONE "
  mkdir ../curriculo-gratis/dist/
  echo "########################################"
  echo "Created dist - DONE "
  sudo cp -r dist/* ../curriculo-gratis/dist/
  echo "########################################"
  echo "Copy and past - DONE "
  cd ../curriculo-gratis
  echo "########################################"
  echo "Enter in directory - DONE "
  git status
  echo "########################################"
  echo "Status - DONE "
  git add .
  echo "########################################"
  echo "Add - DONE "
  git commit -m 'upgrade with shell-script'
  echo "########################################"
  echo "Commit - DONE "
  git push
}
deploy_firebase()
{
  echo "########################################"
  echo "Inicializando deploy firebase"
  sudo rm -rf ../curriculo_gratis-base/public
  echo "########################################"
  echo "Remove public - DONE "
  mkdir ../curriculo_gratis-base/public
  echo "########################################"
  echo "Created public - DONE "
  sudo cp -r dist/* ../curriculo_gratis-base/public/
  echo "########################################"
  echo "Copy and past - DONE "
  cd ../curriculo_gratis-base
  echo "########################################"
  echo "Enter in directory - DONE "
  firebase deploy
  echo "########################################"
  echo "Deploy base - DONE "
}
echo "----------------------------------------"
echo "########################################"
echo "Ola, hoje e " $time_now " ;-)"
echo "Deseja iniciar o programa? y/n"
read resp
if [ $resp == "y" ]
  then
    main
    deploy_heroku
    deploy_firebase
    exit 1
elif [ "$resp" = "heroku" ]
  then
    main
    deploy_heroku
    exit 1
elif [ "$resp" = "firebase" ]
  then
    main
    deploy_firebase
    exit 1
else
    echo "########################################"
    echo "Good bye ;-)"
fi
echo "----------------------------------------"
echo "########################################"
echo "----------------------------------------"
echo "finish"
echo "----------------------------------------"
echo "########################################"
